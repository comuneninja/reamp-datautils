package reamp.ai.data.spreadsheet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import reamp.ai.data.file.ReampFileUtils;
import reamp.ai.utils.S3Utils;
import reamp.ai.utils.TextUtils;

public class WorksheetUtils {

	private static final WorksheetUtils singleton = new WorksheetUtils();

	private TextUtils textUtils = TextUtils.getInstance();

	private ReampFileUtils fileUtils = ReampFileUtils.getInstance();

	private WorksheetUtils() {
	}

	public static WorksheetUtils getInstance() {
		return singleton;
	}
	
	public static void main(String[] args) {
		WorksheetUtils worksheetUtils = WorksheetUtils.getInstance();
		List<Map<String, String>> table = worksheetUtils.tableFromXls("/Users/lugia/dev/temp/camaroso.xls");
		for (Map<String, String> line : table) {
			for (Entry<String, String> entry : line.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();
				
				System.out.println(key + ":" + value);
			}
		}
	}

	private static void createHtmlFoldersForFeed(File refPng, File[] files) {
		for (File file : files) {
			String name = file.getName();
			String absolutePath = file.getAbsolutePath();
			System.out.println(absolutePath);
			if(file.isDirectory()) createHtmlFoldersForFeed(refPng, file.listFiles());
			if(name.contains("index.html")) {
				System.out.println("index.html was found!!");
				try {
					// this is the folder ...
					FileUtils.copyFile(refPng, new File(file.getParentFile().getAbsolutePath() + "/logo.png"));
					String content = FileUtils.readFileToString(file);
					content = content.replaceAll("./img/logo.svg", "logo.png");
					FileUtils.writeStringToFile(file, content);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static void dcoGOV(String[] args) {
		String s = "/Users/megamew/Desktop/queijo.xls";
		
		String outputDir = "/Users/megamew/temp/201705/generatedTemplates";
		
		String templateDir = "/Users/megamew/temp/201705/base/300x250";
		String jsDataFile = templateDir + "/data.json";
		// String s =
		// "/Users/jessica/temp/2015-07/jarvis2/Adinfo_VarejoNacional_maio_v13.xlsx";

		List<Map<String, String>> tableFromXls = WorksheetUtils.getInstance().tableFromXls(s, "DCO-Benefcios", "CODINOME",
				"TEXT01");
		
		TextUtils textUtils = TextUtils.getInstance();
		
		S3Utils s3 = S3Utils.getInstance();
		
		for (Map<String, String> row : tableFromXls) {
			String dimensao = row.get("DIMENSÃO");
			String faseDaCampanha = row.get("FASE DA CAMPANHA");
			String formato = row.get("FORMATO");
			String linhaCriativa = row.get("LINHA CRIATIVA");
			String codigo = row.get("CÓDIGO");
			
			System.out.println(codigo);
			
			String simpleId = textUtils.cleanForId(dimensao + faseDaCampanha + formato + linhaCriativa + codigo);
			String id = textUtils.md5(simpleId);
			
			if(dimensao.equals("300x250") && !formato.contains("Video")) {
				JSONArray jsArray = new JSONArray();
				
				JSONObject o = new JSONObject();
				
				JSONObject txt01 = new JSONObject();
				txt01.put("txt", row.get("TEXT01"));

				JSONObject txt02 = new JSONObject();
				txt02.put("txt", row.get("TEXT02"));

				JSONObject txt03 = new JSONObject();
				txt03.put("txt", row.get("TEXT03"));

				JSONObject txt04 = new JSONObject();
				txt04.put("txt", row.get("TEXT04"));

				JSONObject txt05 = new JSONObject();
				txt05.put("txt", row.get("TEXT05"));

				JSONObject txt06 = new JSONObject();
				txt06.put("txt", row.get("TEXT06"));

				JSONObject txt07 = new JSONObject();
				txt07.put("txt", row.get("TEXT07"));
				
				o.put("txt01", txt01);
				o.put("txt02", txt02);
				o.put("txt03", txt03);
				o.put("txt04", txt04);
				o.put("txt05", txt05);
				o.put("txt06", txt06);
				o.put("txt07", txt07);
				
				jsArray.add(o);
				
				File jsFile = new File("~/" + id + ".json");
				try {
					FileUtils.writeStringToFile(jsFile, jsArray.toJSONString());
					s3.putObject(jsFile, "127349-dco-data-js-" + id + "2.json");
					
					String finalLocation = "http://rme.reamp.com.br/127349-dco-data-js-" + id + "2.json";
					
					File generatedOutputTemplateDir = new File(outputDir + "/" + simpleId);
					File generatedOutputMainJs = new File(outputDir + "/" + simpleId + "/js/main.js");
					
					FileUtils.forceMkdir(generatedOutputTemplateDir);
					FileUtils.copyDirectory(new File(templateDir), generatedOutputTemplateDir);
					
					String mainJsContent = FileUtils.readFileToString(generatedOutputMainJs);
					mainJsContent = mainJsContent.replaceAll("./data.json", finalLocation);
					
					FileUtils.writeStringToFile(generatedOutputMainJs, mainJsContent);
					
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
			}
		}
		
		System.err.println(tableFromXls.size());
	}

	public List<Map<String, String>> tableFromCsv(String csvLocation, String... headerRowNames) {
		List<Map<String, String>> valueMapList = new ArrayList<Map<String, String>>();
		try {
			BufferedReader reader = fileUtils.createReader(csvLocation);
			String line = null;

			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

			fileUtils.close(reader);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return valueMapList;
	}

	public List<String> returnSheetNames(String xlsLocation){
		List<String> sheetList = new ArrayList<>();
		try {

			FileInputStream fileInputStream = new FileInputStream(xlsLocation);
			
			Workbook workbook = null;
			
			if(xlsLocation.endsWith(".xlsx"))
				workbook = new XSSFWorkbook(fileInputStream);
			else
				workbook = new HSSFWorkbook(fileInputStream);
			
			int numberOfSheets = workbook.getNumberOfSheets();

			for (int i = 0; i < numberOfSheets; i++) {
				Sheet worksheet = workbook.getSheetAt(i);
				String sheetName = worksheet.getSheetName();
				sheetList.add(sheetName);
				
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		
		return sheetList;
	}
	
	class WorksheetAnalysis {
		private String dataSheet;
		private String dataHeader;
		public String getDataSheet() {
			return dataSheet;
		}
		public void setDataSheet(String dataSheet) {
			this.dataSheet = dataSheet;
		}
		public String getDataHeader() {
			return dataHeader;
		}
		public void setDataHeader(String dataHeader) {
			this.dataHeader = dataHeader;
		}
	}
	
	public WorksheetAnalysis analyzeWorksheet(String xlsLocation) {
		WorksheetAnalysis wa = new WorksheetAnalysis();
		List<Map<String, String>> valueMapList = new ArrayList<Map<String, String>>();
		try {

			FileInputStream fileInputStream = new FileInputStream(xlsLocation);

			Workbook workbook = null;
			
			if (xlsLocation.contains(".xlsx"))
				workbook = new XSSFWorkbook(fileInputStream);
			else
				workbook = new HSSFWorkbook(fileInputStream);

			int numberOfSheets = workbook.getNumberOfSheets();

			int worksheetCounter = 0;

			for (int i = 0; i < numberOfSheets; i++) {
				Sheet worksheet = workbook.getSheetAt(i);
				String sheetName = worksheet.getSheetName();

				Map<Integer, String> headerRowMap = new HashMap<Integer, String>();

				boolean fieldMapRow = false;
				boolean fieldMapRowFound = false;

				int rowCounter = 0;
				int leftPadding = 0;

				rows: for (Row row : worksheet) {
					Map<String, String> rowMap = new HashMap<String, String>();

					for (Cell cell : row) {
						String stringCellValue;
						try {
							stringCellValue = cell.getStringCellValue();
						} catch (Exception e) {
							stringCellValue = Double.toString(cell.getNumericCellValue());
						}
						
						if(stringCellValue != null && stringCellValue.length() > 0) {
							wa.setDataSheet(sheetName);
							wa.setDataHeader(stringCellValue);
							return wa;
						}
					}
				}
				worksheetCounter++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Map<String, String>> tableFromXls(String xlsLocation) {
		WorksheetAnalysis wa = analyzeWorksheet(xlsLocation);
		return tableFromXls(xlsLocation, wa.getDataSheet(), wa.getDataHeader());
	}
	
	public List<Map<String, String>> tableFromXls(String xlsLocation, String selectSheet, String... headerRowNames) {
		List<Map<String, String>> valueMapList = new ArrayList<Map<String, String>>();
		
		Workbook workbook = null;
		
		try {

			FileInputStream fileInputStream = new FileInputStream(xlsLocation);

			if (xlsLocation.contains(".xlsx"))
				workbook = new XSSFWorkbook(fileInputStream);
			else
				workbook = new HSSFWorkbook(fileInputStream);

			int numberOfSheets = workbook.getNumberOfSheets();
			
			System.out.println(numberOfSheets);

			int worksheetCounter = 0;

			for (int i = 0; i < numberOfSheets; i++) {
				Sheet worksheet = workbook.getSheetAt(i);
				String sheetName = worksheet.getSheetName();
				
				System.out.println(sheetName);

				if (selectSheet != null) {
					// if(LevenshteinDistance.computeLevenshteinDistance(sheetName,
					// selectSheet) > 5) continue;
					if (!sheetName.equalsIgnoreCase(selectSheet))
						continue;
				}

				Map<Integer, String> headerRowMap = new HashMap<Integer, String>();

				boolean fieldMapRow = false;
				boolean fieldMapRowFound = false;

				int rowCounter = 0;
				int leftPadding = 0;

				rows: for (Row row : worksheet) {
					Map<String, String> rowMap = new HashMap<String, String>();

					if (!fieldMapRowFound) {
						for (Cell cell : row) {
							String stringCellValue;
							try {
								stringCellValue = cell.getStringCellValue();
							} catch (Exception e) {
								stringCellValue = Double.toString(cell.getNumericCellValue());
							}

							if (textUtils.isEmpty(stringCellValue) && !fieldMapRowFound)
								leftPadding++;

							if (stringCellValue != null && headerRowCondition(cell, stringCellValue, headerRowNames)
									&& !fieldMapRowFound) {
								fieldMapRow = true;
								fieldMapRowFound = true;
								break;
							}
						}

						if (fieldMapRow) {
							int cellCounter = 0;
							for (Cell cell : row) {
//								if (leftPadding-- > cellCounter)
//									continue;

								String stringCellValue = null;
								try {
									stringCellValue = cell.getStringCellValue();
								} catch (Exception e) {
									try {
										stringCellValue = Double.toString(cell.getNumericCellValue());
									} catch (Exception e2) {
										stringCellValue = "--";
									}
								}

								try {
									if(!textUtils.isEmpty(stringCellValue)) headerRowMap.put(cellCounter, stringCellValue);
								} catch (Exception e) {
									headerRowMap.put(cellCounter, "--");
								} finally {
									cellCounter++;
								}
							}
						}

					} else {

						for (int cellCounter = 0; cellCounter < headerRowMap.size(); cellCounter++) {
							Cell cell = row.getCell(cellCounter);

							String stringCellValue = null;
							if (cell != null) {
								try {
									stringCellValue = cell.getStringCellValue();
								} catch (Exception e) {
									try {
										Date dateCellValue = cell.getDateCellValue();
										if (!HSSFDateUtil.isCellDateFormatted(cell)) throw new RuntimeException();
										stringCellValue = new SimpleDateFormat("dd/MM/yyyy").format(dateCellValue);
									} catch (Exception e2) {
										try {
											stringCellValue = Double.toString(cell.getNumericCellValue());
										} catch (Exception e3) {
											stringCellValue = "";
										}
									}
								}
							} else {
								stringCellValue = "";
							}

							String fieldName = headerRowMap.get(cellCounter);
							rowMap.put(textUtils.isEmpty(fieldName) ? "--" : fieldName.trim(), stringCellValue);

						}

						String rowString = textUtils.printValues(rowMap);
						String match = textUtils.getGroup(rowString, "([0-9]+)", 1);
						if (match == null || match.length() == 0) {
							break rows;
						}

						valueMapList.add(rowMap);
					}

					rowCounter++;
					if (!fieldMapRowFound)
						leftPadding = 0;
				}
				worksheetCounter++;

				String headerString = textUtils.printValues(headerRowMap);
				// System.out.println(headerString);

				for (Map<String, String> map : valueMapList) {
					// System.out.println(map.toString());
					System.out.println(textUtils.printKeys(map));
					break;
				}

				for (Map<String, String> map : valueMapList) {
					// System.out.println(map.toString());
					System.out.println(textUtils.printValues(map));
				}
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				workbook.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return valueMapList;
	}

	private boolean headerRowCondition(Cell cell, String stringCellValue, String... headerRowNames) {
		return textUtils.nearToOneOfThese(stringCellValue, headerRowNames);
	}

}
