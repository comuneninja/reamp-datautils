package reamp.ai.data.file;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

public class ReampFileUtils {

	private static ReampFileUtils instance = new ReampFileUtils();

	private ReampFileUtils() {
	}

	public String readFile(String filePath) throws FileNotFoundException {
		BufferedReader reader = null;
		StringBuilder fileString = null;

		try {
			File file = new File(filePath);
			reader = new BufferedReader(new FileReader(file));

			if (reader != null)
				fileString = new StringBuilder();

			String line;

			while ((line = reader.readLine()) != null) {
				fileString.append(line);
				fileString.append("\n");
			}

		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {

		} finally {
			close(reader);
		}

		return fileString.toString();
	}

	public BufferedReader createReader(String filePath) throws FileNotFoundException {
		BufferedReader reader = null;

		try {
			File file = new File(filePath);
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			throw e;
		}

		return reader;
	}

	public void close(BufferedReader reader) {
		try {
			reader.close();
		} catch (Exception e) {

		}
	}

	public static ReampFileUtils getInstance() {
		return instance;
	}

	public boolean externalFileExist(String url) {
		String path = "/reamp/jarvis/temp/xpto-" + new Date().getTime();
		URL link;
		try {
			link = new URL(url);
			InputStream in = new BufferedInputStream(link.openStream());
			ByteArrayOutputStream out = new ByteArrayOutputStream();

			byte[] buf = new byte[1024];

			int n = 0;

			while (-1 != (n = in.read(buf))) {
				out.write(buf, 0, n);
			}

			out.close();
			in.close();

			byte[] response = out.toByteArray();

			File file = new File(path);

			FileOutputStream fos = new FileOutputStream(file);

			fos.write(response);
			fos.close();

			return true;

		} catch (Exception e) {
			// e.printStackTrace();
			System.out.println("Não existe");
		}
		return false;
	}

	public File downloadFile(String url, String fileName, String path) {

		URL link;
		File file = null;
		try {
			link = new URL(url);
			InputStream in = new BufferedInputStream(link.openStream());
			ByteArrayOutputStream out = new ByteArrayOutputStream();

			byte[] buf = new byte[1024];

			int n = 0;

			while (-1 != (n = in.read(buf))) {
				out.write(buf, 0, n);
			}

			out.close();
			in.close();

			byte[] response = out.toByteArray();

			file = new File(path+fileName);

			FileOutputStream fos = new FileOutputStream(file);

			fos.write(response);
			fos.close();

		} catch (Exception e) {
			e.printStackTrace();
			// System.out.println("Não existe");
		}

		return file;

	}

	public File downloadFile(String url) {
		String path = "/reamp/jarvis/temp/xpto-" + new Date().getTime();
		URL link;
		File file = null;
		try {
			link = new URL(url);
			InputStream in = new BufferedInputStream(link.openStream());
			ByteArrayOutputStream out = new ByteArrayOutputStream();

			byte[] buf = new byte[1024];

			int n = 0;

			while (-1 != (n = in.read(buf))) {
				out.write(buf, 0, n);
			}

			out.close();
			in.close();

			byte[] response = out.toByteArray();

			file = new File(path);

			FileOutputStream fos = new FileOutputStream(file);

			fos.write(response);
			fos.close();

		} catch (Exception e) {
			e.printStackTrace();
			// System.out.println("Não existe");
		}

		return file;
	}

}
