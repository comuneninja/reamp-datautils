package reamp.ai.utils;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextUtils implements Serializable {

	private static TextUtils INSTANCE = null;
	private static final int CHUNK_SIZE = 2000;
	private static Pattern metaPattern = Pattern.compile("<meta\\s+([^>]*http-equiv=\"?content-type\"?[^>]*)>", Pattern.CASE_INSENSITIVE);
	private static Pattern charsetPattern = Pattern.compile("charset=\\s*([a-z][_\\-0-9a-z]*)", Pattern.CASE_INSENSITIVE);
	
//	private RandomUtil rand = RandomUtil.getInstance();

	private TextUtils() {

	}

	public static TextUtils getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new TextUtils();
		}
		return INSTANCE;
	}
	
	public String getFirstNotEmpty(String def, Map<String,String> params, String... options) {
		String firstNotEmpty = getFirstNotEmpty(params, options);
		return firstNotEmpty != null ? firstNotEmpty : def;
	}
	
	public String getFirstNotEmpty(Map<String,String> params, String... options) {
		for (String option : options) {
			String v = params.get(option);
			if(v != null) return v;
		}
		return null;
	}
	
	public String ifDifferentThenSet(String original, String set, String... values) {
		for (String v : values) {
			if(v.equals(original)) return original;
		}
		return set;
	}

	public String printf(String t, String... vars) {
		Formatter formatter = new Formatter();
		Formatter r = formatter.format(t, vars);
		String s = r.toString();
		return s;
	}

	public String encode(String text, String encoding) {
		return new String(text.getBytes(Charset.forName(encoding)));
	}

	public boolean match(String text, String regex) {
		Pattern galeryRootPattern = Pattern.compile(regex);
		Matcher galeryRootPatternMatcher = galeryRootPattern.matcher(text);
		return galeryRootPatternMatcher.find();
	}

	public String getGroup(String text, String regex, int gnumber, int maxLength) {
		String value = "";
		Pattern galeryRootPattern = Pattern.compile(regex);
		Matcher galeryRootPatternMatcher = galeryRootPattern.matcher(text);
		while (galeryRootPatternMatcher.find()) {
			if (maxLength != -1 && value.length() <= maxLength) {
				value = galeryRootPatternMatcher.group(gnumber);
				continue;
			}
			value = galeryRootPatternMatcher.group(gnumber);
		}
		return value;
	}

	public String getGroup(String text, String regex, int gnumber) {
		String value = "";
		Pattern galeryRootPattern = Pattern.compile(regex);
		Matcher galeryRootPatternMatcher = galeryRootPattern.matcher(text);
		while (galeryRootPatternMatcher.find()) {
			value = galeryRootPatternMatcher.group(gnumber);
		}
		return value;
	}
	
	public String replaceVars(String text, Map<String,String> varMap) {
		Set<String> keySet = varMap.keySet();
		for (String key : keySet) {
			String value = varMap.get(key);
			text = text.replaceAll("<<"+key+">>", value);
		}
		return text;
	}

	public List<String> getListByRegex(String content, String regex, Integer gn) {
		List<String> valueList = new ArrayList<String>();
		Pattern galeryRootPattern = Pattern.compile(regex);
		Matcher galeryRootPatternMatcher = galeryRootPattern.matcher(content);

		while (galeryRootPatternMatcher.find()) {
			String value = galeryRootPatternMatcher.group(gn);
			valueList.add(value);
		}
		return valueList;
	}

	public String retrieveByRegex(String content, String xpath, Integer gn) {
		List<String> valueList = new ArrayList<String>();
		Pattern galeryRootPattern = Pattern.compile(xpath);
		Matcher galeryRootPatternMatcher = galeryRootPattern.matcher(content);

		while (galeryRootPatternMatcher.find()) {
			String value = galeryRootPatternMatcher.group(gn);
			valueList.add(value);
		}

		return valueList.get(0);
	}

	public Float getNumericFloatValue(String value) throws Exception {
		try {
			float parseFloat = Float.parseFloat(value);
			return parseFloat;
		} catch (Exception e) {
			try {
				int parseInt = Integer.parseInt(value);
				return (float) parseInt;
			} catch (Exception e2) {
				throw new Exception(value + " could bot be parsed to a float nor an integer", e2);
			}
		}
	}
	
	public Float parseForFloatValue(String value) throws Exception {
		try {
			float parseFloat = Float.parseFloat(value);
			return parseFloat;
		} catch (Exception e) {
			try {
				// check if it has both commas and dots
				int comma = value.indexOf(",");
				int dot = value.indexOf(".");
				if (comma != -1 && dot != -1) {
					if (comma < dot) {
						String before = value.substring(0, comma);
						String after = value.substring(comma + 1);
						value = before + after;
					} else {
						String before = value.substring(0, dot);
						String after = value.substring(dot + 1);
						value = before + after;
					}
				} else {
					// check for multiple commas or dots
					if (comma != value.lastIndexOf(",")) {
						String before = value.substring(0, comma);
						String after = value.substring(comma + 1);
						value = before + after;
					}
					if (dot != value.lastIndexOf(".")) {
						String before = value.substring(0, dot);
						String after = value.substring(dot + 1);
						value = before + after;
					}
				}

				// replace commas and dots
				value = value.replaceAll(",", ".");
				float parseFloat = Float.parseFloat(value);
				return parseFloat;
			} catch (Exception e2) {
				try {
					int parseInt = Integer.parseInt(value);
					return (float) parseInt;
				} catch (Exception e3) {
					return null;
				}
			}
		}
	}

	public String stringFromCollection(Collection<String> wordList) {
		StringBuilder sb = new StringBuilder("");
		for (String region : wordList) {
			sb.append(region + ",");
		}
		if (wordList.size() > 0) {
			return sb.substring(0, sb.lastIndexOf(","));
		}
		return sb.toString();
	}

	public String stringFromObjectCollection(Collection<Object> objectList) {
		StringBuilder sb = new StringBuilder("");
		for (Object value : objectList) {
			String stringValue = simpleObjectToString(value);

			sb.append(stringValue + ",");
		}
		if (objectList.size() > 0) {
			return sb.substring(0, sb.lastIndexOf(","));
		}
		return sb.toString();
	}

	public String simpleObjectToString(Object value) {
		String stringValue = "";
		// if(value instanceof Float) {
		// // numeric formating
		// } else if(value instanceof Long) {
		//
		// } else if(value instanceof Integer) {
		//
		// } else if(value instanceof Date) {
		//
		// }
		stringValue += value.toString();
		return stringValue;
	}

	public List<String> buildListFromString(String text, String separator) {
		List<String> resultList = new ArrayList<String>();
		if (!isEmpty(text)) {
			String[] splitRestrictions = new String[] {};
			if (text.indexOf(separator) != -1) {
				splitRestrictions = text.split(separator);
			} else {
				splitRestrictions = new String[] { text };
			}
			for (String restriction : splitRestrictions) {
				if (!isEmpty(restriction)) {
					resultList.add(restriction.trim());
				}
			}
		}
		return resultList;
	}
	
	public List<String> buildListFromString(String text) {
		return buildListFromString(text, ",");
	}

	public String groupReplace(String text, String regex, String groupReplace) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		return matcher.replaceAll(groupReplace);
	}

	public static Set<String> compare(Set<String> nossoDescontoSet, Set<String> ourSet) {
		Set<String> diffSet = new HashSet<String>();
		for (String site : nossoDescontoSet) {
			if (!ourSet.contains(site)) {
				diffSet.add(site);
			}
		}
		return diffSet;
	}

	private static String sniffCharacterEncoding(byte[] content) {
		int length = content.length < CHUNK_SIZE ? content.length : CHUNK_SIZE;

		// We don't care about non-ASCII parts so that it's sufficient
		// to just inflate each byte to a 16-bit value by padding.
		// For instance, the sequence {0x41, 0x82, 0xb7} will be turned into
		// {U+0041, U+0082, U+00B7}.
		String str = "";
		try {
			str = new String(content, 0, length, Charset.forName("ASCII").toString());
		} catch (UnsupportedEncodingException e) {
			// code should never come here, but just in case...
			return null;
		}

		Matcher metaMatcher = metaPattern.matcher(str);
		String encoding = null;
		if (metaMatcher.find()) {
			Matcher charsetMatcher = charsetPattern.matcher(metaMatcher.group(1));
			if (charsetMatcher.find())
				encoding = new String(charsetMatcher.group(1));
		}

		return encoding;
	}

	public String md5(String senha) {
		String sen = "";
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
		sen = hash.toString(16);
		return sen;
	}

	public boolean isEmpty(String s) {
		return s == null || s.equals("");
	}

	public String formatNumber(double random, int precision) {
		return formatNumber(random, precision, '.');
	}
	
	public String formatNumber(double random, int precision, char sep) {
		DecimalFormat decimalFormat = new DecimalFormat();
		DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance();
		decimalFormatSymbols.setDecimalSeparator(sep);
		decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);
		NumberFormat formatter = decimalFormat;
		formatter.setGroupingUsed(false);
		formatter.setMaximumFractionDigits(precision);
		return formatter.format(random);
	}
	
	public static void main(String[] args) {
		TextUtils tu = TextUtils.getInstance();
		System.out.println(tu.cleanUrl("discrepância"));
	}

	public String cleanUrl(String v) {
		return Normalizer.normalize(v, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}
	
	public String cleanForId(Object v) {
		if(v == null) return null;
		return cleanForId(v.toString());
	}
	
	public String cleanForId(String v) {
		if(v == null) return null;
		return v.toLowerCase().replaceAll("[^0-9A-Za-z]", "");
	}
	
	public String cleanForMapKey(String v) {
		if(v == null) return null;
		return v.replace(" ", "_").toLowerCase().replaceAll("[^0-9A-Za-z_]", "");
	}
	
	public String cleanForMapKeyTolerant(String v) {
		if(v == null) return null;
		return v.replace(" ", "_").toLowerCase().replaceAll("[^:0-9A-Za-z_]", "");
	}
	
	public String cleanForJson(String v) {
		if(v == null) return null;
		return v.replaceAll("[^\\x20-\\x7A]", "");
	}

	public String cleanFloat(String v) {
		String newValue = v;
		int firstDot = v.lastIndexOf(".");
		int firstComma = v.indexOf(",");

		if (firstDot != -1) {
			int afterFirstDot = v.length() - firstDot;
			if (afterFirstDot > 3) {
				newValue = v.replace(".", "");
			}
			if (firstComma != -1) {
				if (firstComma > firstDot) {
					// remove dot
					newValue = v.replace(".", "");
				} else {
					newValue = v.replace(",", "");
				}
			}
		}

		if (newValue.endsWith(".") || newValue.endsWith(",")) {
			newValue += "0";
		}

		return newValue;
	}

	public String camelize(String v) {
		String separator = " ";
		String[] tokens = v.split(separator);
		String camelized = "";
		StringBuilder sb = new StringBuilder("");
		for (String token : tokens) {
			if (token.length() <= 2 || !shouldCamelize(token)) {
				sb.append(token + separator);
				continue;
			}
			String firstLetter = token.substring(0, 1);
			String rest = token.substring(1, token.length());
			String camelizedToken = firstLetter.toUpperCase() + rest;
			sb.append(camelizedToken + separator);
		}
		camelized = sb.toString().trim();
		return camelized;
	}

	private boolean shouldCamelize(String token) {
		List<String> tList = Arrays.asList(new String[]{
				"dos",
				"das",
		});
		for (String t : tList) {
			if(token.equalsIgnoreCase(t)) return false;
		}
		return true;
	}

	public boolean isNumericFloat(String lat) {
		try {
			float parseFloat = Float.parseFloat(lat);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// public String retrieveSiteFromUrl(String url) {
	// if(url.contains("testOffers")) return "comune";
	// if(url.contains("amazonaws")) return "comune";
	// if(url.contains("groupalia")) return "groupalia";
	// if(url.contains("groupon")) {
	// if(url.contains("groupon.com.br")) return "groupon_brasil";
	// if(url.contains("groupon.com")) return "groupon";
	// return "groupon_world";
	// }
	// try {
	// String host = new URL(url).getHost();
	// TextUtils textUtils = TextUtils.getInstance();
	// System.out.println(host);
	// host = host.replaceAll("w{3}\\.", "");
	// host = host.replaceAll("www2\\.", "");
	// host = textUtils.getGroup(host, "(\\w+)\\..*", 1);
	// return host;
	// } catch (Exception e) {
	//  
	// }
	// return null;
	// }

	public String retrieveSiteFromUrl(String url) {

		if (url.contains("testOffers"))
			return "comune";
		if (url.contains("assine.abril"))
			return "assine.abril";
		if (url.contains("ofertaunica"))
			return "ofertaunica";
		if (url.contains("groupon.com.br"))
			return "groupon_brasil";
		if (url.contains("netshoes.com.br"))
			return "netshoes";

		if (url.contains("comprafacil.com.br"))
			return "comprafacil";

		URL u = null;
		try {
			u = new URL(url);
			String host = u.getHost();

			String regex = "(\\w+)\\.net";
			String domain = getGroup(host, regex, 1);
			if (!isEmpty(domain))
				return domain;

			regex = "(\\w+)\\.com";
			domain = getGroup(host, regex, 1);
			if (!isEmpty(domain))
				return domain;

			regex = "(\\w+)\\.com\\.br";
			domain = getGroup(host, regex, 1);
			if (!isEmpty(domain))
				return domain;

			regex = "(\\w+)\\.(\\w+)\\..+";
			String subdomain = getGroup(host, regex, 1);
			domain = getGroup(host, regex, 2);
			return domain;
		} catch (Exception e) {
			 
		}
		return "comune";
	}

	public boolean isXml(String url) {
		return url.contains("xml");
	}

	public String addUtmToUrl(String url) throws Exception {
		url = url.trim();
		System.out.println(url);
		if (url.contains("utm_source=Comune")) {
			do {
				url = url.replaceAll("\\?utm_source=Comune\\&", "\\?");
			} while (url.contains("?utm_source=Comune&"));

			url = url.replaceAll("\\?utm_source=Comune", "");

			url = url.replaceAll("&utm_source=Comune", "").replaceAll("utm_source=Comune", "");
		}

		if (url.contains("utm_source=comune")) {
			do {
				url = url.replaceAll("\\?utm_source=comune\\&", "\\?");
			} while (url.contains("?utm_source=comune&"));

			url = url.replaceAll("\\?utm_source=comune", "");

			url = url.replaceAll("&utm_source=comune", "").replaceAll("utm_source=comune", "");
		}

		String c = "?";
		if (url.contains("?")) {
			// use &
			c = "&";
		}
		if (!url.endsWith(c)) {
			url += c;
		}
		url += "utm_source=Comune";
		System.out.println(url);
		return url;
	}

	public String addParameterToUrl(String url, String parName, String parValue) throws Exception {
		System.out.println(url);

		if (url.contains(parName + "=")) {
			do {
				url = url.replaceAll("\\?" + parName + "=" + "[^\\&]+" + "\\&", "\\?");
			} while (match(url, "\\?" + parName + "=" + "[^\\&]+" + "\\&"));

			url = url.replaceAll("\\?" + parName + "=" + "[^\\&]+" + "", "");

			url = url.replaceAll("\\&" + parName + "=" + "[^\\&]+" + "", "").replaceAll("" + parName + "=" + "[^\\&]+" + "", "");
		}

		String c = "?";
		if (url.contains("?")) {
			// use &
			c = "&";
		}
		if (!url.endsWith(c)) {
			url += c;
		}
		url += "" + parName + "=" + parValue + "";
		System.out.println(url);
		return url;
	}

	public String addParameterToUrlOld(String url, String parName, String parValue) throws Exception {
		System.out.println(url);

		if (url.contains(parName + "=" + parValue)) {
			do {
				url = url.replaceAll("\\?" + parName + "=" + parValue + "\\&", "\\?");
			} while (url.contains("?" + parName + "=" + parValue + "&"));

			url = url.replaceAll("\\?" + parName + "=" + parValue + "", "");

			url = url.replaceAll("&" + parName + "=" + parValue + "", "").replaceAll("" + parName + "=" + parValue + "", "");
		}

		if (url.contains("" + parName + "=" + parValue + "")) {
			do {
				url = url.replaceAll("\\?" + parName + "=" + parValue + "\\&", "\\?");
			} while (url.contains("?" + parName + "=" + parValue + "&"));

			url = url.replaceAll("\\?" + parName + "=" + parValue + "", "");

			url = url.replaceAll("&" + parName + "=" + parValue + "", "").replaceAll("" + parName + "=" + parValue + "", "");
		}

		String c = "?";
		if (url.contains("?")) {
			// use &
			c = "&";
		}
		if (!url.endsWith(c)) {
			url += c;
		}
		url += "" + parName + "=" + parValue + "";
		System.out.println(url);
		return url;
	}

	public String genRan() {
		String ran = Integer.toString((int) (System.currentTimeMillis())) + "-" + Integer.toString((int) (Math.random() * 100));
		return ran;
	}

	public String simpleRan() {
		SimpleDateFormat sdf = new SimpleDateFormat("hh-mm-ss");
		String time = sdf.format(new Date());
		String ran = time;
		return ran;
	}

	public String sumAndHash(String... values) {
		StringBuilder sb = new StringBuilder("");
		for (String v : values) {
			sb.append(v);
		}
		return md5(sb.toString());
	}

	

	public String printCollection(List<String> filteredList) {
		StringBuilder sb = new StringBuilder("");
		for (String f : filteredList) {
			sb.append(f);
			sb.append(",");
		}
		return sb.toString();
	}

	public Map<String, String> paramMapFromArgs(String[] args) {
		Map<String, String> paramMap = new HashMap<String, String>();
		for (int i = 0; i < args.length; i++) {
			String a = args[i];
			if (a.startsWith("-")) {
				String fieldName = a.substring(1, a.length());
				String value = args[++i];
				// log.info(fieldName + ":" + value);
				paramMap.put(fieldName, value);
			}
		}
		return paramMap;
	}

	public String addParameterToUrl(String url, String par) throws Exception {
		System.out.println(url);
		String p = par;
		if (url.contains(p)) {
			do {
				url = url.replaceAll("\\?" + p + "\\&", "\\?");
			} while (url.contains("?" + p + "&"));

			url = url.replaceAll("\\?" + p, "");

			url = url.replaceAll("&" + p, "").replaceAll(p, "");
		}

		String pl = p.toLowerCase();
		if (url.contains(pl)) {
			do {
				url = url.replaceAll("\\?" + pl + "\\&", "\\?");
			} while (url.contains("?" + pl + "&"));

			url = url.replaceAll("\\?" + pl, "");

			url = url.replaceAll("&" + pl, "").replaceAll(pl, "");
		}

		String c = "?";
		if (url.contains("?")) {
			// use &
			c = "&";
		}
		if (!url.endsWith(c)) {
			url += c;
		}
		url += p;
		System.out.println(url);
		return url;
	}
	
	public String cut(String original, int max) {
		StringBuilder sb = new StringBuilder("");
		String[] tokenList = original.split(" ");
		for (String token : tokenList) {
			if(sb.length() + 1 + token.length() > max) break;
			sb.append(token + " ");
		}
		String result = sb.toString().trim();
		if(result.length() < original.length()) result = result + "...";
		
		return result;
	}

	public String nearToOneOfThese(Map<String,String> params, String... string) {
		//LevenshteinDistance ld = LevenshteinDistance.computeLevenshteinDistance(str1, str2)
		
		for (Entry<String, String> entry : params.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			
			int lengthPercentage = (int) (key.length() * 0.2);
			
			for (String s : string) {
				if(s == null) continue;
				int ld = LevenshteinDistance.computeLevenshteinDistance(s.toLowerCase(), key.toLowerCase());
				if(ld < lengthPercentage) return value;
			}
		}
		
		return null;
	}
	
	public boolean nearToOneOfThese(String param, String... string) {
		for (String s : string) {
			if(isNear(s, param)) return true;
		}
		return false;
	}
	
	public boolean isNear(String token, String jobToken) {
		boolean calc = LevenshteinDistance.computeLevenshteinDistance(token.toLowerCase(), jobToken.toLowerCase()) < Math.ceil(token.length() * 0.2);
		//System.out.println(token + " to " + jobToken + " : " + calc);
		return calc;
	}
	
	public int calculateProximity(String token, String jobToken) {
		return LevenshteinDistance.computeLevenshteinDistance(token.toLowerCase(), jobToken.toLowerCase());
	}
	
	public int calculateProximityIgnoreWhitespace(String token, String jobToken) {
		return LevenshteinDistance.computeLevenshteinDistance(token.toLowerCase().replaceAll(" ", ""), jobToken.toLowerCase().replaceAll(" ", ""));
	}

	public <K,V> String printValues(Map<K,V> fieldMap) {
		StringBuilder sb = new StringBuilder();
		
		for (Entry<K, V> entry : fieldMap.entrySet()) {
			Object value = entry.getValue();
			
			sb.append(value + ",");
		}
		
		String headerString = sb.toString();
		return headerString;
	}
	
	public <K,V> String printKeys(Map<K,V> fieldMap) {
		StringBuilder sb = new StringBuilder();
		
		for (Entry<K, V> entry : fieldMap.entrySet()) {
			Object key = entry.getValue();
			
			sb.append(key + ",");
		}
		
		String headerString = sb.toString();
		return headerString;
	}

	public String printMap(Map<String, String> map) {
		StringBuilder sb = new StringBuilder();
		
		for (Entry<String, String> entry : map.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			
			sb.append(key + ":" + value + ";");
		}
		
		return sb.toString();
	}

	public List<String> getLines(String coreMessage) {
		if(coreMessage == null) return null;
		String[] split = coreMessage.split("\n");
		return Arrays.asList(split);
	}

	public boolean endsWith(String nomenclatura, String... endings) {
		for (String end : endings) {
			if(nomenclatura.endsWith(end)) return true;
		}
		return false;
	}
	
	public boolean endsWithIgnoreCase(String nomenclatura, String... endings) {
		for (String end : endings) {
			if(nomenclatura.toLowerCase().endsWith(end.toLowerCase())) return true;
		}
		return false;
	}

	public boolean isOneOf(String tecnologia, String... values) {
		for (String val : values) {
			if(tecnologia.trim().toLowerCase().equals(val.trim().toLowerCase())) return true;
		}
		return false;
	}

//	public String randomString() {
//		return rand.random(0, 99999) + "";
//	}

	public boolean containsAnyOfThese(String reference, String[] campaignNameFilters) {
		for (String name : campaignNameFilters) {
			if(reference.toLowerCase().contains(name.toLowerCase())) return true;
		}
		return false;
	}

	public String extractToken(String separator, int index, String str){
		try{
			return str.split(separator)[index];
		}catch(Exception ex){
			return null;
		}
	}
	
	public String removeAccents(String s){
		return Normalizer.normalize(s, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}
}
